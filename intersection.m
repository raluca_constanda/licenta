function [out] = intersection(input_path, output_path)

	series = load(input_path);
	out = load(output_path);

	for i=1:length(series)
		if(series(i) ~= 0)
			out(i)++;
		end
	end

	% write to file
	file_id = fopen(output_path, 'w');
	fprintf(file_id, '%d ',out);

	fclose(file_id);

end