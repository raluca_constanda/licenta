<?php

    $inputsDirectory = "inputs/";
    $dataFileNames = array("afraid", "alive", "angry", "confused", "depressed", "good", "happy", "helpless", "hurt", "indifferent","interested", "love", "open", "positive", "sad", "strong"); 
    // $dataFileNames = array("strong");
    foreach ($dataFileNames as $name) {

        $dataFileName = $inputsDirectory . $name;
    	$file = fopen($dataFileName, "r") or die("Unable to open file!");
    	
    	$fileContent = fread($file,filesize($dataFileName));
    	
    	$listOfFeelings = explode(',', $fileContent);

    	fclose($file);

    	$url = 'https://books.google.com/ngrams/graph?';
        
        $urlParams = '';
        $year_start = 1500;
        $year_end = 2008;
        $smoothing = 1;
        
        //15 English
        //17 American English
        //18 British English
        $corpus = 17;

        $direct_url = '';

        for ($i = 0; $i < count($listOfFeelings); $i++) {
            $urlParams .= $listOfFeelings[$i];
        	$direct_url .= "t1%3B%2C" . $listOfFeelings[$i];
            if($i != count($listOfFeelings) - 1) {
                $urlParams .= '%2C';
                $direct_url .= '%3B%2Cc0%3B.';
            } else {
            	$direct_url .= '%3B%2Cc0';
            }
        }
        $url .= "content=" . $urlParams . '&';
        $url .= "year_start=" . $year_start . '&';
        $url .= "year_start=" . $year_end . '&';
        $url .= "corpus=" . $corpus . '&';
        $url .= "smoothing=" . $smoothing . '&';
        $url .= "share=&";
        $url .= "direct_url=" . $direct_url;

        $url = str_replace("\n", '', $url);
echo $url;
    	$ch = curl_init();
    	$params = array(
    		CURLOPT_HEADER => TRUE,
    		CURLOPT_URL => $url,
    		CURLOPT_RETURNTRANSFER => 1
    	);

    	curl_setopt_array($ch, $params);
    	$result = curl_exec($ch);
    	if($result === false) {
    		echo "Error curl". curl_error($ch);
    		return;
    	}

    	$result = substr($result, strpos($result, "var data = ") + strlen("var data = "));    
        $result = explode(';',$result);   
        $ngrams = json_decode($result[0], true); 
        foreach($ngrams as $ngram) {
        	
        	$fileName = 'outputs_american/' . $name . '/' . $ngram['ngram'];
        	$file = fopen($fileName, "w");

        	foreach ($ngram['timeseries'] as $key => $value) {
        		$row = $value * 1000000 . "\n";
    			fwrite($file, $row);
        	}

        	fclose($file);
        }

    	curl_close($ch);
    }
