function [reg] = regress_ksegments(series, weights, k)

    N = length(series);

    [one_seg_dist,one_seg_mean] = prepare_ksegments(series, weights);

    k_seg_dist = zeros(k, N+1);
    k_seg_path = zeros(k, N);

    k_seg_dist(1,2:end) = one_seg_dist(1,:);

    k_seg_path(1,:) = 0;
    k_seg_path(sub2ind(size(k_seg_path),1:k,1:k)) = (1:k) - 1;

    for p=2:k
        for n=p:N
            choices = k_seg_dist(p-1, 1:n) + one_seg_dist(1:n, n)';

            [bestval,bestidx] = min(choices);
            k_seg_path(p,n) = bestidx - 1;
            k_seg_dist(p,n+1) = bestval;

        end
    end

    reg = zeros(size(series));
k_seg_path
k_seg_dist
    rhs = length(reg);
    for p=k:-1:1
rhs
        lhs = k_seg_path(p,rhs);
lhs
        reg(lhs+1:rhs) = one_seg_mean(lhs+1,rhs);
reg
        rhs = lhs;
    end
end