#!/bin/bash

for directoryName in fts_british/segments_british/outputs_british/* ; do
	for filename in $directoryName/result ; do
			# echo $filename;
			octave --silent --eval "periods_array_format(\"$filename\",\"$directoryName/periods_array_format\")";
			echo 'Finish' $filename;
	done
done
