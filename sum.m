function [N] = sum(input_path, output_path)

	series = load(input_path);
	suma = load(output_path);
	N=length(series);

	suma = suma + series;

	file_id = fopen(output_path, 'w');
	fprintf(file_id, '%d ',suma);

	fclose(file_id);
end
