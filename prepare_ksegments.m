function [dists, means] = prepare_ksegments(series, weights)
    N = length(series);

    wgts = diag(weights);
    wsum = diag(weights .* series);
    sqrs = diag(weights .* series .* series);

    dists = zeros(N,N);
    means = diag(series);

    for gama=1:N
        for l=1:(N-gama)
            r = l + gama;

            wgts(l,r) = wgts(l,r-1) + wgts(r,r);
            wsum(l,r) = wsum(l,r-1) + wsum(r,r);
            sqrs(l,r) = sqrs(l,r-1) + sqrs(r,r);

            means(l,r) = wsum(l,r) / wgts(l,r);
            dists(l,r) = sqrs(l,r) - means(l,r)*wsum(l,r);        
        end
    end

end