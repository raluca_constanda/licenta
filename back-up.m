function [reg] = k_segmentation()

 	dataFileNames = ['afraid' 'alive' 'angry' 'confused' 'depressed' 'good' 'happy' 'helpless' 'hurt' 'indifferent' 'interested' 'love' 'open' 'positive'
 		'sad' 'strong']; 
    

	series_british = load('outputs_british/afraid/afraid');
	series_american = load('outputs_american/afraid/afraid');
	N=length(series_american);

	weights(1:N) = 1/N;

	[reg_american] = regress_ksegments(series_american', weights, 20);
	[reg_british] = regress_ksegments(series_british', weights, 20);
	

	% teta = (max(reg) - min(reg)) / (N / 20)
	% teta = 10;
	% first_index = 1;
	% last_index = 1;
	% mid_index = 1;	
	% nb_of_segments = 2;
	% average = 0;

	% for i=1:(N-1)
	% 	if (reg(i) ~= reg(i+1))

	% 		if(abs(reg(i)-reg(i+1)) <= teta)

	% 			if (first_index == mid_index)
	% 				average = (reg(i) + reg(i+1))/2;
	% 				mid_index = i;
	% 			else

	% 				average = (nb_of_segments * average + reg(i+1) ) / (nb_of_segments + 1);
	% 				mid_index = i;
	% 				nb_of_segments = nb_of_segments + 1;
	% 			end
	% 		end
	% 		if((abs(reg(i)-reg(i+1)) > teta) && (first_index ~= mid_index))
	% 				reg(first_index:i) = average;
	% 				first_index = i+1;
	% 				mid_index = i+1;
	% 				nb_of_segments = 2;
	% 		end
	% 	end
	% end

	% if(first_index == 1)
	% 	reg(1:N) = average;
	% end
	% plot(reg_british)
	% print('reg_british.txt');
	% plot(reg_american)
	% print('reg_american.txt');
	save 'reg_british.txt' reg_british
	save 'reg_american.txt' reg_american

	load('reg_american.txt');
	reg
end