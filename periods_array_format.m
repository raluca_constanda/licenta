function [N] = periods_array_format(input_path, output_path)

	series = load(input_path);
	N=length(series);
	min_words = 3;
	% intervals = zeros(N);

	for i=1:N
		
		if(series(i) > min_words)
			intervals(i) = 1;
		else
			intervals(i) = 0;
		end

	end

	% write to file
	file_id = fopen(output_path, 'w');
	fprintf(file_id, '%d ',intervals);

	fclose(file_id);

end
