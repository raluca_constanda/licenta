function [series] = periods(input_path, output_path)

	series = load(input_path);
	N=length(series);
	min_words = 5;
	first = 0;
	last = 0;
	intervals = zeros(N,2);

	k=1;
	for i=1:N
		
		if(series(i) > min_words && first == 0)
			first = i+1499;
		end
		if(series(i) < min_words && first ~= 0)
			last = (i - 1) + 1500;
		end
		if(first ~= 0 && last ~= 0) 
			intervals(k, 1) = first;
			intervals(k, 2) = last;
			first = 0;
			last = 0;
			k++;
		end

	end
	intervals(1:k-1, :)
	% write to file
	file_id = fopen(output_path, 'w');
	fprintf(file_id, '%d ',intervals(1:k-1, :)');

	fclose(file_id);

end
