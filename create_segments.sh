#!/bin/bash

for directoryName in outputs_american/* ; do
	for filename in $directoryName/* ; do
			echo $filename;
			octave --silent --eval "k_segmentation(\"$filename\",\"segments_american/$filename\")";
			echo 'Finish' $filename;
	done
done
