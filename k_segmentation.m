function [reg1] = k_segmentation(input_path)

	series = load(input_path);
	N=length(series);

	weights(1:N) = 1/N;
	[reg] = regress_ksegments(series', weights, 20);
	
	% write to file
	% file_id = fopen(output_path, 'w');
	% fprintf(file_id, '%f ',reg);

	x=(1500:2008);
	grid on;
	plot(x,reg,'LineWidth',2);
	legend('alarmed');
	% fclose(file_id);
	print 'alarmed'

end