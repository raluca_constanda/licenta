\select@language {romanian}
\contentsline {section}{\numberline {0.1}Rezumat}{3}{section.0.1}
\contentsline {chapter}{\numberline {1}Introducere}{6}{chapter.1}
\contentsline {section}{\numberline {1.1}Limbajul \IeC {{\textcommabelow s}}i via\IeC {{\textcommabelow t}}a social\IeC {\u a}}{6}{section.1.1}
\contentsline {section}{\numberline {1.2}Scopul lucr\IeC {\u a}rii}{7}{section.1.2}
\contentsline {chapter}{\numberline {2}Google Ngrams}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Scurt istoric}{8}{section.2.1}
\contentsline {section}{\numberline {2.2}N-grame}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Google Books Ngram Viewer}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Interfa\IeC {{\textcommabelow t}}a}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Cazuri de utilizare}{10}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Controverse \IeC {{\textcommabelow s}}i concluzii}{11}{subsection.2.3.3}
\contentsline {chapter}{\numberline {3}Studiul literaturii din domeniu}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Societatea analizat\IeC {\u a} pe baza schimb\IeC {\u a}rilor climei}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Testarea economisirii societ\IeC {\u a}\IeC {{\textcommabelow t}}ii}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Analiz\IeC {\u a} de sentimente bazat\IeC {\u a} pe colec\IeC {{\textcommabelow t}}ia de date Twitter}{13}{section.3.3}
\contentsline {section}{\numberline {3.4}Exprimarea emo\IeC {{\textcommabelow t}}iilor \IeC {\^\i }n secolul al XX-lea}{14}{section.3.4}
\contentsline {chapter}{\numberline {4}Datele \IeC {{\textcommabelow s}}i algoritmii aplica\IeC {{\textcommabelow t}}i}{16}{chapter.4}
\contentsline {section}{\numberline {4.1}Setul ini\IeC {{\textcommabelow t}}ial de date}{17}{section.4.1}
\contentsline {section}{\numberline {4.2}Extragerea frecven\IeC {{\textcommabelow t}}elor din Google Ngrams}{18}{section.4.2}
\contentsline {section}{\numberline {4.3}Algoritmi aplica\IeC {{\textcommabelow t}}i}{19}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Algoritmul de segmentare "k-segmentation"}{19}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Descriere algoritm}{19}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Rezultate algoritm}{21}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Determinarea perioadelor \IeC {\^\i }n care sentimentele au fost mai intense}{21}{section.4.4}
\contentsline {section}{\numberline {4.5}Determinarea sentimentelor dominante pentru perioada 1500 - 2008}{23}{section.4.5}
\contentsline {section}{\numberline {4.6}Testarea algoritmului de segmentare}{24}{section.4.6}
\contentsline {section}{\numberline {4.7}Perioadele de apari\IeC {{\textcommabelow t}}ie a sentimentelor}{26}{section.4.7}
\contentsline {section}{\numberline {4.8}Compararea rezultatelor ob\IeC {{\textcommabelow t}}inute pentru vocabularul englez cu cele ob\IeC {{\textcommabelow t}}inute pentru vocabularul american}{26}{section.4.8}
\contentsline {chapter}{\numberline {5}Rezultate finale \IeC {{\textcommabelow s}}i concluzii}{28}{chapter.5}
\contentsline {section}{\numberline {5.1}Determinarea perioadelor \IeC {\^\i }n care o clas\IeC {\u a} de sentimente a fost dominant\IeC {\u a}}{28}{section.5.1}
\contentsline {section}{\numberline {5.2}Determinarea sentimentelor dominante pentru perioada 1500 - 2008}{30}{section.5.2}
\contentsline {section}{\numberline {5.3}Perioadele \IeC {\^\i }n care au ap\IeC {\u a}rut sentimentele}{32}{section.5.3}
\contentsline {section}{\numberline {5.4}Corpusul britanic vs corpusul american}{33}{section.5.4}
\contentsline {section}{\numberline {5.5}Studii viitoare}{33}{section.5.5}
\contentsline {section}{\numberline {5.6}Concluzii}{34}{section.5.6}
\contentsline {chapter}{\numberline {A}Sentimentele dominante (1500 - 2008)}{35}{appendix.A}
\contentsline {chapter}{Lista de figuri}{38}{table.A.1}
\contentsline {chapter}{Bibliografie}{39}{appendix*.2}
