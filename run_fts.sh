#!/bin/bash

# for directoryName in segments_american/outputs_american/* ; do
# 	for filename in $directoryName/* ; do
# 			echo $filename;
# 			octave --silent --eval "fts(\"$filename\",\"fts_american/$filename\")";
# 			echo 'Finish' $filename;
# 	done
# done
for directoryName in segments_british/outputs_british/* ; do
	for filename in $directoryName/* ; do
			echo $filename;
			octave --silent --eval "fts(\"$filename\",\"fts_british/$filename\")";
			echo 'Finish' $filename;
	done
done
