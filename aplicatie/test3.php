<?php 

	$action = $_GET['action'];

	if(strcmp($action, "getWords") == 0){
		$className = $_GET['className'];
		$fileName = "feelings_inputs/" . $className; 

		$file = fopen($fileName, "r") or die("Unable to open file!");
		
		$fileContent = fread($file, filesize($fileName));
		fclose($file);

		$listOfFeelings = explode(',', $fileContent);

		echo json_encode($listOfFeelings);

		exit();
	}

	if(strcmp($action, "input") == 0){
		$path = $_GET['word'];
		$className = explode('-', $path)[0];
		$word = explode('-', $path)[1];

		//british
		$fileNameBritish = "outputs_british/$className/$word"; 
		$file = fopen($fileNameBritish, "r") or die("Unable to open file!");
		$fileContentBritish = fread($file, filesize($fileNameBritish));
		// fclose($file);
		
		$listOfFeelingsBritish = explode('\n', $fileContentBritish);

		// american
		$fileNameAmerican = "outputs_american/$className/$word"; 
		$file1 = fopen($fileNameAmerican, "r") or die("Unable to open file!");
		$fileContentAmerican = fread($file1, filesize($fileNameAmerican));
		// fclose($file);
		
		$listOfFeelingsAmerican = explode('\n', $fileContentAmerican);


		// $response = {};
		$response['american'] = $listOfFeelingsAmerican;
		$response['british']  = $listOfFeelingsBritish;


		echo json_encode($response);

		exit();
	}

	