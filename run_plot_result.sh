#!/bin/bash

for directoryName in fts_british/segments_british/outputs_british/* ; do
	echo "$directoryName/image.png";
	octave --silent --eval "plot_result(\"$directoryName/result\", \"$directoryName/image.png\")";
done
