function [series] = fts(input_path, output_path)

	series = load(input_path);
	N=length(series);

	average = min(series)+((max(series) - min(series))*8/10);

	for i=1:N
		if(series(i) < average)
			series(i) = 0;
		end
	end
	
	% write to file
	file_id = fopen(output_path, 'w');
	fprintf(file_id, '%f ',series);

	fclose(file_id);

end
