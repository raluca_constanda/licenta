function [out] = plot_result(input_path, output_path)

	series = load(input_path);
output_path
	x=(1500:2008);
	grid on;
	% print -dpng -color output_path

	graphics_toolkit gnuplot 
	plot(x, series);
	xlabel('Time');
        
    print output_path 
end